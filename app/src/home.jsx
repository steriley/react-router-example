import { Link } from 'react-router';

const Home = () =>
  <div>
    <p>Homepage</p>
    <Link to="profile" className="btn">Go to Profile</Link>
  </div>
;

module.exports = Home;
