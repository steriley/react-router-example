import { Link } from 'react-router';

const Profile = () =>
  <div>
    <p>Profile page!</p>
    <Link to="/" className="btn">Go to Home</Link>
  </div>
;

module.exports = Profile;
