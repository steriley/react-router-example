#Start Server
npm start

#WebPack
Watch and build when a change happens:

    $ npm run watch

Build without the watch

    $ npm run build

Build with a minified version of bundle

    $ npm run dist

#ES Lint File Source
https://gist.github.com/ghostwords/40936f11091b87987e56

http://eslint.org/docs/rules/
